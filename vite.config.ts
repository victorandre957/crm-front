import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import aliases from "./aliases.config.js";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte()],

  resolve: {
    alias: aliases,
  },

  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@use "$styles/variables.scss" as *;',
      },
    },
  },

  /**
   * As this project is a SPA hash based (using #),
   * the base path can be relative to work in servers like gitlab pages,
   * where the site assets is not served at the domain root.
   *
   * Using Base as empty the vite will solve the asset path relative.
   */
  base: "",
});
