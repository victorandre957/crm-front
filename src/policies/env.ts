export const isDevelopmentEnv = () => import.meta.env.VITE_USER_NODE_ENV === "development";

export const isStagingEnv = () => import.meta.env.VITE_USER_NODE_ENV === "staging";

export const isProductionEnv = () => import.meta.env.VITE_USER_NODE_ENV === "production";
