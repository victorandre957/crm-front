import type { StorybookConfig } from "@storybook/svelte-vite";
const config: StorybookConfig = {
  stories: ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx|svelte)"],
  addons: ["@storybook/addon-essentials", "@storybook/addon-interactions"],
  framework: {
    name: "@storybook/svelte-vite",
    options: {},
  },
  docs: {
    autodocs: true,
  },
  refs: {
    "uai-components": {
      title: "Uai-components",
      url: "https://vlgi.github.io/uai-components",
    },
  },
};
export default config;
