import type { Preview } from "@storybook/svelte";
import viewports from "./viewports";

import "$styles/app.scss";
import "uai-components/styles";

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    viewport: {
      viewports,
    },
    refs: {
      "uai-components": {
        title: "Uai-components",
        url: "https://vlgi.github.io/uai-components",
      },
    },
  },
};

export default preview;
