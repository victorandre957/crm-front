# Svelte SPA template

This template should help get you started developing with Svelte SPA.

## Recommended IDE Setup

Use [VS Code](https://code.visualstudio.com/) and install the
[extensions recommended for the workspace](./.vscode/extensions.json). Make sure the
[settings](./.vscode/settings.json) aren't obscured by your user settings in any way.

## Setting up the environment

1. Instal `docker`
2. If in windows install `WSL2`
3. Run `make up` to get inside the container
4. Inside de container run `npm install`

## Make commands

```sh
# Docker commands
make up     # Start the container and get inside the app container
make down   # Stop the container
make enter  # Get inside the container (useful to get more than one terminal inside the app container)
```

## Running

```sh
# For development
npm run dev         # Runs the app in hot reload mode
npm run storybook   # Runs storybook in hot reloading mode

# For deployment
npm run build           # Builds the app into the dist folder
npm run storybook-build # Builds a static storybook site into storybook-static
```

To run storybook from the static folder you'll have to host the storybook-static folder.

## Testing

You may run two kinds of tests:

```sh
# run once variants
npm run test:vitest # Runs tests in *.test.ts files
npm run test:storybook # Runs tests in storybook stories using test runner

# watch variants (will rerun changed tests automatically)
npm run test:vitest-watch # Runs tests in *.test.ts files
npm run test:storybook-watch # Runs tests in storybook stories using test runner
```

## Linting and Formatting

The project comes with a pre-loaded eslint configuration, prettier and svelte-check. The commands
are as follows:

```sh
npm run format # Formats all files using prettier
npm run lint-fix # Fixes all autofixable issues detected by eslint
npm run format-check # Checks if it's formatter correctly using prettier
npm run lint # Checks if the files are okay according to eslint
npm run check # Checks if the files are okay according to svelte-check
```

## Testing everything

You may want to run all the tests to check if the project is following the formatter, linter and has
no failing tests. You can do that with the command `npm run check-all`

## How is it structured

```
/
|-.storybook/
| |-main.ts         # Storybook configuration
| |-preview.ts      # Storybook render configuration
| |-viewport.ts     # All the pre-loaded viewports
|
|-.vscode/
| |-extensions.json # Extension recommendations
| |-settings.json   # Workspace settings
|
|-public/           # This is copied to the root of the Svelte app
|-scripts/          # Scripts that help with build and other stuff
|-src/
| |-assets/     # add all your assets here
| |-components/ # add all your svelte components here, one folder per component
| | |-Component/                 # Should have the same name as the component
| |   |-Component.svelte         # The component should be capitalized
| |   |-Component.stories.ts     # We recommend normal CSF
| |   |-Component.test.ts        # The tests for this component
| |   |-*other files related to the component*
| |
| |-helpers/    # add any useful code that's not specific here
| |-pages/      # Add your pages here
| | |-*same structure as components*
| |
| |-policies/   # Add route policies here. You can use in other places too.
| |-stores/     # Add globally initialized stores here
| |-stories/    # Add any dangling documentation here as *.mdx here
| |-styles/     # All globally available *.css or *.scss files should be put here
| |-types/      # Useful type definitions or type templates should be put here
| |
| |-App.svelte    # The root component
| |-main.ts       # The starting point of the app
| |-routes.ts     # The app routes definition and route policies handler
| |-env.d.ts      # Add type for env variables
| |-vite-env.d.ts # Add type references as needed
|
|-.editorconfig     # Configure the IDE with the project's format
|-.eslintrc.cjs     # Customize your eslint settings
|-.prettierrc.cjs   # Customize your prettier settings
|-aliases.config.js # All the aliases available in the project. Add more here
|-index.html        # The root of the website
|-Makefile          # File to create short hands commands to run outside the app container
|-package.json      # All dependencies here
|-svelte.config.js  # Configure how svelte is built
|-tsconfig.json     # Configure the typescript
|-vite.config.ts    # Configure how the app builds
|-vitest.config.ts  # Configure how the app is tested
|-.env              # Configure common variables see more in https://vitejs.dev/guide/env-and-mode.html#env-files
```

## Snippets

This project has defined some snippets to help with svelte and stories boilerplate.

### Snippets for svelte file

- `component`: Create Svelte Component with TS and SCSS
- `script`: Create Svelte Script tag with TS lang
- `style`: Create Svelte Style tag with SCSS lang

### Snippets for Typescript file

- `story`: Create a story for a component.
- `story-template`: Create a story for a component with story template, to reuse common configs.
